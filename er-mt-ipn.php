<?php
/**
 * Created by Alessandro Morelli <development@alphac.it>.
 * Copyright AlphaC s.r.l.
 *
 */

foreach( array(__DIR__ . '/../../../wp-config.php', __DIR__ . '/../wordpress/wp-config.php') as $file) {
	if(file_exists($file)) {
		define('WPCONFIGFILE', $file);
		break;
	}
}
require $file;

$wp->init(); $wp->parse_request(); $wp->query_posts();
$wp->register_globals(); $wp->send_headers();

$option = get_option('reservations_mt_options');
$reservation_support_mail = get_option('reservations_support_mail');

$return_url = $option['return_url'];
if(class_exists('TranslationShunt')) {
	$shunt = new TranslationShunt();
	$return_url = $shunt->get_post_translation($return_url);
}
$thanks = get_permalink( $return_url );

if(!$option || !is_array($option) || !isset($option['modus']) || $option['modus'] === 'off') {
	wp_mail($reservation_support_mail, 'Error with MT IPN Callback', 'MT Gateway not enabled');
	exit;
}

if(wp_verify_nonce($_POST['custom'], 'easy-pay-submit' )) {

	easyreservations_mt_callback($_POST['invoice'],$_POST['amount'], $option);
	wp_safe_redirect($thanks);
} else {
	wp_mail($reservation_support_mail, 'Error with MT IPN Callback', 'Wrong nonce: '.$_POST['custom']);
}

function easyreservations_mt_callback($invoice_nr, $amount, $mt_options){
	$autosave = get_option('reservations_autoapprove');
	if(strpos($invoice_nr, '-') === false){
		$res = new Reservation((int) $invoice_nr);
		$array = '';
		$array[] = 'pricepaid';
		$res->updatePricepaid(0);
		if($autosave == 2 || $autosave == 3){
			$approve = easyreservations_auto_approve($res, false);
			if($approve){
				$res->status = 'yes';
				$array[] = 'status';
				$array[] = 'resourcenumber';
			}
		}
		$array[] = 'custom';
		$res->Customs(array(
			'type' => 'mt', 'value' => array(
				'single' => true,
				'required_amount' => $amount,
				'description' => $invoice_nr
			)
		), true, false,false,'mt');
		$res->editReservation($array, false, array('reservations_email_to_admin_paypal', 'reservations_email_to_user_paypal'), array(false, $res->email));
	} else {
		$explode = explode('-', $invoice_nr);
		$total = count($explode) - 1;
		$totalpaid = $amount;
		$index = 0;
		$count = count($explode);
		foreach($explode as $key => $id){
			$res = new Reservation((int) $id);
			$res->Calculate();
			if($res->price <= $totalpaid){
				if($key == $total) {
					$paid = $totalpaid;
				}
				$paid = $res->price;
			} elseif($totalpaid > 0) {
				$paid = $totalpaid;
			} else {
				$paid = 0;
			}
			$totalpaid -= $res->price;
			$array = '';
			$array[] = 'pricepaid';
			$res->updatePricepaid(0);
			if($res->status != 'yes' && ($autosave == 2 || $autosave == 3)){
				$approve = easyreservations_auto_approve($res, false);
				if($approve){
					$res->status = 'yes';
					$array[] = 'status';
					$array[] = 'resourcenumber';
				}
			}
			$array[] = 'custom';
			$res->Customs(array(
				type => 'mt', 'value' => array(
					'single' => false,
					'index' => $index,
					'count' => $count,
					'required_amount' => $amount,
					'description' => $invoice_nr
				)
			), true, false,false,'mt');
			$res->editReservation($array, false, array('reservations_email_to_admin_paypal', 'reservations_email_to_user_paypal'), array(false, $res->email));
		}
	}
}
