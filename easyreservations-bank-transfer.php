<?php

/*
Plugin Name: EasyReservations Bank Transfer
Description: Money transfer payment gateway.
Version: 1.2.6
Author: AlphaC <development@alphac.it>
License: proprietary
Bitbucket Plugin URI: https://bitbucket.org/alphac/easyreservation-bank-transfer
*/

/**
 * @param PHPMailer $phpmailer
 */
function fix_multi_emails($phpmailer) {
	$body = $phpmailer->Body;
	if(is_array($body)) {
		foreach ($body as $type => $part) {
			if ($type === 'text/html') {
				$phpmailer->Body = $part;
			} else if ($type === 'text/plain') {
				$phpmailer->AltBody = $part;
			} else {
				$phpmailer->addAttachment($body, '', 'base64', $type);
			}
		}
	}
}
add_action('phpmailer_init', 'fix_multi_emails');

function register_my_custom_gateway($gateways){
	$gateways['mt'] = array(
		'name' => __('Money Transfer', 'easyReservationsMT'),
		'form_name' => 'mt_gateway_form',
		'amount_name' => 'amount'
	);
	return $gateways;
}

function my_gateways_settings(){
	wp_enqueue_media();
	$options = get_option( 'reservations_mt_options' );
	$popt = array(
		'echo' => 0,
		'name' => 'er_mt_return'
	);
	if ( isset( $options['return_url'] ) && is_numeric($options['return_url'])) {
		$popt['selected'] = $options['return_url'];
	}

	$pImage = $options['image'];

	$pButton =<<<HTML
<div class="uploader">
	<input id="mt_payment_image" name="er_mt_button" type="text" value="{$pImage}"/>
	<input id="mt_payment_image_button" class="button" name="er_mt_button_loader" type="button" value="Upload" data-uploader_title="Choose image" data-uploader_button_text="Insert URL" />
	<script>
jQuery(document).ready(function(){
// Uploading files
var file_frame;

  jQuery('#mt_payment_image_button').live('click', function( event ){

    event.preventDefault();

    // If the media frame already exists, reopen it.
    if ( file_frame ) {
      file_frame.open();
      return;
    }

    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });

    // When an image is selected, run a callback.
    file_frame.on( 'select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();

      // Do something with attachment.id and/or attachment.url here
      jQuery('#mt_payment_image').val(attachment.url);
    });

    // Finally, open the modal
    file_frame.open();
  });
});
</script>
</div>
HTML;

	$bank = __('Bank', 'easyReservationsMT');

	$rows  = array(
		__( 'Mode', 'easyReservationsMT' ) => easyreservations_generate_input_select( 'er_mt_modus', array(
			'off' => __( 'Off', 'easyReservationsMT' ),
			'on'  => __( 'On', 'easyReservationsMT' )
		), $options['modus'], 'style="width:100px"' ),
		__( 'After success return to', 'easyReservationsMT' ) => ' ' . __('Page', 'easyReservationsMT') . ': ' . wp_dropdown_pages($popt),
		__( 'Payment prefix', 'easyReservationsMT') => '<input type="text" name="er_mt_prefix" value="'. $options['prefix'] .'">',
		__( 'Bank', 'easyReservationsMT') => '<input type="text" name="er_mt_bank" value="'. $options['bank'] .'">',
		__( 'IBAN', 'easyReservationsMT') => '<input type="text" name="er_mt_iban" value="'. $options['iban'] .'">',
		__( 'BIC/SWIFT', 'easyReservationsMT') => '<input type="text" name="er_mt_bic" value="'. $options['bic'] .'">',
		__( 'Account Holder', 'easyReservationsMT') => '<input type="text" name="er_mt_holder" value="'. $options['holder'] .'">',
		__( 'Payment button', 'easyReservationsMT') => $pButton,
		__( 'Payment button as text', 'easyReservationsMT') =>'<input type="checkbox" name="er_mt_button_text" value="1"'. ($options['button_text'] === true ? ' checked ' : '') .'">',
	);
	$table = easyreservations_generate_table( 'reservation_mt_settings_table', __('Money Transfer', 'easyReservationsMT') . ' ' . __( 'settings', 'easyReservationsMT' ), $rows, 'style="margin-top:7px;width:99%"' );
	echo easyreservations_generate_form( 'reservation_mt_settings', 'admin.php?page=reservation-settings&site=pay#reservation_mt_settings', 'post', false, array(
		'easy-set-authorize' => wp_create_nonce( 'easy-set-authorize' ),
		'action'             => 'reservation_mt_settings'
	), $table );
}
add_action('reservations_gateway_settings', 'my_gateways_settings');

function reservations_save_mt_gateway_settings() {
	if($_POST['action'] == "reservation_mt_settings") {
		$options = array(
			'modus'      => $_POST['er_mt_modus'],
		);

		if ( isset( $_POST['er_mt_return'] ) ) {
			if ( is_numeric($_POST['er_mt_return'] ) ) {
				$options['return_url'] = $_POST['er_mt_return'];
			} else {
				$options['return_url'] = null;
			}
		}
		if( isset($_POST['er_mt_iban']) ) {
			$options['iban'] = $_POST['er_mt_iban'];
		} else {
			$options['iban'] = null;
		}
		if( isset($_POST['er_mt_button_text']) && $_POST['er_mt_button_text'] === '1') {
			$options['button_text'] = true;
		} else {
			$options['button_text'] = false;
		}
		if( isset($_POST['er_mt_prefix']) ) {
			$options['prefix'] = $_POST['er_mt_prefix'];
		} else {
			$options['prefix'] = null;
		}
		if( isset($_POST['er_mt_bank']) ) {
			$options['bank'] = $_POST['er_mt_bank'];
		} else {
			$options['bank'] = null;
		}
		if( isset($_POST['er_mt_bic']) ) {
			$options['bic'] = $_POST['er_mt_bic'];
		} else {
			$options['bic'] = null;
		}
		if( isset($_POST['er_mt_holder']) ) {
			$options['holder'] = $_POST['er_mt_holder'];
		} else {
			$options['holder'] = null;
		}
		if( isset($_POST['er_mt_button']) ) {
			$options['image'] = $_POST['er_mt_button'];
		} else {
			$options['image'] = null;
		}

		update_option( 'reservations_mt_options', $options );
		echo '<br><div class="updated"><p>' . __('Money Transfer', 'easyReservationsMT') . ' ' . __( 'settings changed', 'easyReservationsMT' ) . '</p></div>';
	}
}
add_action('reservations_save_gateway_settings', 'reservations_save_mt_gateway_settings');

add_filter('reservations_register_gateway', 'register_my_custom_gateway', 10, 1);

function generate_mt_gateway_payment_form($res,$id,$title,$price,$nonce) {

	$options = get_option('reservations_mt_options');


	//Open form with link to gateway; the name of this should be the value of form_name at gateway registration
	$form = "<form name=\"mt_gateway_form\" action=\"". plugins_url('er-mt-ipn.php', __FILE__) . "\" method=\"post\" id=\"easy_mt_form\">";

	$form .= '<div class="money-transfer-details">';
	$form .= mt_format_details($options, true, true);
	$form .= '<br>' . __('These bank coordinates will also be included in the confirmation email', 'easyReservationsMT') . '<br>';
	$form .= '</div>';
	//Add a image as button to click on
	if($options['button_text'] === true) {
		$form .= '<input type="submit" value="' . __('Pay with a Wire Transfer', 'easyReservationsMT') . '">';
	} else {
		$form .= '<input type="image" src="' . $options['image'] . '" border="0" name="submit" alt="' . __( 'Bank Transfer', 'easyReservationsMT' ) . '">';
	}
	//Create array with the fields required by gateway
	$array = array(
		//The id required for the ipn function is the variable $id
		'invoice' => $id,

		//Description is the variable $title
		'item_name' => $title,

		//Field for price; It's key should be the value of amount_name at gateway registration
		'amount' => $price,

		//If the gateway supports a custom field use it for the nonce and check it in the ipn
		'custom' => wp_create_nonce('easy-pay-submit'),

		//Some examples that are required in most gateways in some form
	);

	//Generate hidden fields from the array
	$form .= easyreservations_generate_hidden_fields($array);
	//Close form
	$form .= '</form>';

	return $form;
}
add_filter('reservations_generate_gateway_button', 'generate_mt_gateway_payment_form', 10, 5);

function mt_gateway_easy_email_tag($theForm, $fields, $field, $reservation)
{
	if($field[0] === 'mtdetails') {
		$options = get_option('reservations_mt_options');
		$details = easyreservations_get_mt_infos($reservation);

		if(!is_array($options) || !$details) {
			$theForm=preg_replace('/\['.$fields.']/U', '', $theForm);

			return $theForm;
		}

		$text = '';

		if($details['single'] === true) {
			$text .= __( 'To complete the reservation, please issue a wire transfer', 'easyReservationsMT' ) . '<br>';
		} else {
			$text .= sprintf(__('Reservation %1d of %2d','easyReservationsMT'), $details['index'], $details['count']) . '<br>';
			$text .= __( 'To complete the reservations, please issue a single wire transfer', 'easyReservationsMT' ) . '<br>';
		}

		$text .= __( 'Amount required', 'easyReservationsMT' ) . ': ' . $details['required_amount'] . '<br>';
		$text .= __( 'Payment description', 'easyReservationsMT' ) . ': ' . $options['prefix'] . ' ' . $details['description'] . '<br>';

		$text .= mt_format_details( $options, true );
		$theForm=preg_replace('/\['.$fields.']/U', $text, $theForm);
	}

	return $theForm;
}

function mt_format_details($options, $html = true, $css = false) {
	if($html) {
		$br = '<br>';
	} else {
		$br = '\n';
	}

	$details = '';
	if($options['holder']) {
		$details .= mt_decorate( $css, 'money-transfer-label', __( 'Account Holder', 'easyReservationsMT' ) . ': ' ) .
		            mt_decorate( $css, 'money-transfer-value', $options['holder'] ) .
		            $br;
	}

	if($options['bank']) {
		$details .= mt_decorate( $css, 'money-transfer-label', __( 'Bank', 'easyReservationsMT' ) . ': ' ) .
		            mt_decorate( $css, 'money-transfer-value',$options['bank'] ) .
		            $br;
	}

	$details .= mt_decorate($css, 'money-transfer-label', __('IBAN', 'easyReservationsMT') . ': ') .
	            mt_decorate($css, 'money-transfer-value', $options['iban']);

	if($options['bic']) {
		$details .= $br .
		            mt_decorate( $css, 'money-transfer-label', __( 'BIC/SWIFT', 'easyReservationsMT' ) . ': ' ) .
		            mt_decorate( $css, 'money-transfer-value', $options['bic'] ) . '<br>';
	}

	return $details;
}

function mt_decorate($css, $class, $text) {
	if($css) {
		return "<span class=\"{$class}\">{$text}</span>";
	} else {
		return $text;
	}
}

add_filter('easy-email-tag', 'mt_gateway_easy_email_tag', 10, 4);

add_filter('easy-res-view-table-bottom', 'easyreservations_mt_view', 9, 1);

function easyreservations_mt_view($res){
	$details = easyreservations_get_mt_infos($res);
	if($details){
		?><tr class="alternate">
		<td nowrap><img style="vertical-align:text-bottom;" src="<?php echo RESERVATIONS_URL; ?>images/card.png"> <?php printf ( __( 'Bank Transfer' , 'easyReservationsMT' ));?></td>
		<td><?php printf ( __( 'Yes' , 'easyReservationsMT' ));?></td>
		</tr>
		<?php
	} else {
		?><tr class="alternate">
		<td nowrap><img style="vertical-align:text-bottom;" src="<?php echo RESERVATIONS_URL; ?>images/card.png"> <?php printf ( __( 'Bank Transfer' , 'easyReservationsMT' ));?></td>
		<td><?php printf ( __( 'No' , 'easyReservationsMT' ));?></td>
		</tr>
		<?php

	}
}

add_action('easy-dash-edit-side-middle', 'easyreservations_mt_box', 10, 1);

function easyreservations_mt_box($res){
	$details = easyreservations_get_mt_infos($res);

	if($details) {
		$mt = __('Yes', 'easyReservationsMT');
	} else {
		$mt = __('No', 'easyReservationsMT');
	}
	?><table class="<?php echo RESERVATIONS_STYLE; ?>" id="easy_edit_mt" style="min-width:320px;width:320px;margin-bottom:4px">
	<thead>
	<tr>
		<th><?php echo __( 'Bank Transfer' , 'easyReservationsMT' );?></th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td style="vertical-align: middle;"><p style="text-align: center; font-size: 200%;"><?php echo $mt; ?></p></td>
	</tr>
	</tbody>
	</table>
	<?php
}

/**
 * @param Reservation $res
 *
 * @return array|bool
 */
function easyreservations_get_mt_infos($res){
	if(!empty($res->custom)){
		$details = $res->getCustoms($res->custom, 'mt');
		$details = array_shift($details);
		if($details) {
			return $details['value'];
		} else {
			return false;
		}
	}
	return false;
}

function easyreservations_mt_init_language() {
	if(isset($_GET['lang'])){
		global $sitepress;
		if($sitepress && is_object($sitepress)) $sitepress->switch_lang($_GET['lang']);
	}
	load_plugin_textdomain('easyReservationsMT', false, dirname(plugin_basename( __FILE__ )).'/languages/' );
}

add_action('plugins_loaded','easyreservations_mt_init_language');

