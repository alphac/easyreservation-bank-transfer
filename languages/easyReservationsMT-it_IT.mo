��          �   %   �      P     Q     `     x  	   �     �     �     �     �     �     �     �     �     �     �     �     �                /  F   F  9   �  A   �     	            �  '     �     �     �     �            &      	   G     Q     c  	   f     p     w     ~     �      �     �     �     �  D     3   I  ;   }     �     �     �             	                            
                                                                                    Account Holder After success return to Amount required BIC/SWIFT Bank Bank Transfer IBAN Mode Money Transfer No Off On Page Pay with a Wire Transfer Payment button Payment button as text Payment description Payment prefix Reservation %1d of %2d These bank coordinates will also be included in the confirmation email To complete the reservation, please issue a wire transfer To complete the reservations, please issue a single wire transfer Yes settings settings changed Project-Id-Version: easyReservationsMT
POT-Creation-Date: 2015-08-25 14:47+0200
PO-Revision-Date: 2015-08-25 14:48+0200
Last-Translator: 
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
 Titolare del conto Pagina di destinazione Importo richiesto Codice BIC/SWIFT: Banca Bonifico bancario Codice IBAN                          : Modalità Bonifico bancario No Disattivo Attivo Pagina Paga con un bonifico Pulsante di pagamento Pulsante di pagamento come testo Causale Bonifico Causale Bonifico Prenotazione %1d di %2d Le coordinate bancarie saranno accluse anche all’email di conferma Per completare la prenotazione, inviate un bonifico Per completare le prenotazioni, inviate un singolo bonifico Sì impostazioni impostazioni salvate 